package by.epamlab.collection;

import by.epamlab.collection.interfaces.Cache;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class LruTest {
    private Cache cache;

    public LruTest() {
        this.cache = new LruCache(2);
    }

    @Test
    public void testCacheStartEmpty() {
        assertEquals(cache.contains(1), false);
    }

    @Test
    public void testContainsTrue() {
        cache.put(1, 1);
        assertEquals(cache.contains(1), true);
    }

    @Test
    public void testCapacti() {
        cache.put(1, 1);
        assertEquals(cache.get(1), 1);
        assertEquals(cache.get(2), null);
        cache.put(2, 4);
        assertEquals(cache.get(1), 1);
        assertEquals(cache.get(2), 4);
    }

    @Test
    public void testCapacityReachedOldestRemoved() {
        cache.put(1, 2);
        cache.put(2, 3);
        cache.put(1, 4);
        assertEquals(cache.get(1), 4);
        assertEquals(cache.get(2), 3);
    }

    @Test
    public void testRemoveCoretc() {
        cache.put(1, 1);
        assertTrue(cache.remove(1));
    }


    @Test
    public void testRemoveElementNotfound() {
        assertFalse(cache.remove(1));
    }

    @Test
    public void testSize() {
        cache.put(1, 2);
        cache.put(2, 2);
        cache.put(3, 3);
        assertEquals(cache.size(), 2);
    }

    @Test
    public void testPutIfAbsentKeyNull() {
        assertEquals(cache.putIfAbsent(7, 8), null);
    }

    @Test
    public void testPutIfAbsentHaveKey() {
        cache.put(1, 2);
        cache.put(2, 5);
        assertEquals(cache.putIfAbsent(1, 2), 2);
    }


}

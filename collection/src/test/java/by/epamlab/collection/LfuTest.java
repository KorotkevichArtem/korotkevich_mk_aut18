package by.epamlab.collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import by.epamlab.collection.interfaces.Cache;
import org.junit.Test;

public class LfuTest {
    private Cache cache;

    public LfuTest() {
        this.cache = new LfuCache(2);
    }

    @Test
    public void testCapacity() {
        cache.put(1, 1);
        cache.put(2, 2);
        assertEquals(cache.get(1), 1);
        assertEquals(cache.get(2), 2);
        cache.put(3, 3);
        assertEquals(cache.get(1), null);
        assertEquals(cache.get(3), 3);
    }

    @Test
    public void testCorectSize(){
        cache.put(1,1);
        cache.put(2,1);
        assertEquals(cache.size(), 2);

    }

    @Test
    public void testContainsTrue(){
        cache.put(1,1);
        assertTrue(cache.contains(1));
        assertFalse(cache.contains(5));
    }

    @Test
    public void testPutIfAbsentKeyNull(){
        assertEquals(cache.putIfAbsent(7,8), null);
    }

    @Test
    public void testPutIfAbsentHaveKey(){
        cache.put(1,1);
        cache.put(2,5);
        assertEquals(cache.putIfAbsent(1,1),1);
    }

    @Test
    public void testRemoveHaveKey(){
        cache.put(1,1);
        assertTrue(cache.remove(1));
    }

    @Test
    public void testRemoveDontHaveKey(){
        assertFalse(cache.remove(1));
    }
}

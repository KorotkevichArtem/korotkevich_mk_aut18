package by.epamlab.collection;

import by.epamlab.collection.interfaces.Cache;

import java.util.Iterator;
import java.util.LinkedHashMap;

/**
 * A simple LRU cache
 */

/**
 * @author Korotkevich Artem
 */

public class LruCache<K, V> implements Cache<K, V> {
    //initial capacity
    private int capacti;
    // a hash map holding <Object, Object> pairs
    private LinkedHashMap<K, V> cacheMap;

    /**
     * Constructs an empty insertion-ordered LRUConcurrentCache instance with the specified initial capacity
     *
     * @param capacti
     */

    public LruCache(int capacti) {
        this.capacti = capacti;
        this.cacheMap = new LinkedHashMap<>();
    }

    @Override
    public boolean contains(K key) {//O(1)
        return cacheMap.containsKey(key);
    }

    /**
     * Set key to hold the value.
     * If key already holds a value, it is overwritten.
     *
     * @param key   the key of the pair
     * @param value the value of the pair
     */
    @Override
    public V put(K key, V value) {//O(1)
        if (!contains(key)) {
            if (this.cacheMap.size() == capacti) {
                remove();
            }
        }
        return cacheMap.put(key, value);
    }

    public void remove() {
        Iterator<?> iterator = this.cacheMap.keySet().iterator();
        iterator.next();
        iterator.remove();
    }

    /**
     * Get the value of key.
     * If the key does not exist, return -1.
     *
     * @param key the key to query
     * @return the value of the key
     */
    @Override
    public V get(K key) {//O(1)
        if (contains(key)) {
            Object value = cacheMap.remove(key);
            cacheMap.put(key, (V) value);
            return cacheMap.get(key);
        } else {
            return null;
        }
    }

    /**
     * deletes values ​​by its key
     *
     * @param key key values ​​to be deleted
     * @return boolean flag true or false
     */
    @Override
    public boolean remove(K key) {//O(n)
        if (!cacheMap.containsKey(key)){
            return false;
        } else {
            cacheMap.remove(key);
            return true;
        }
    }

    /**
     * The thread safe method works on the put principle
     *
     * @param key
     * @param value
     * @return key
     */
    @Override
    public V putIfAbsent(K key, V value) {//O(1)
        if (!cacheMap.containsKey(key)) {
            return cacheMap.put(key, value);
        }
        return cacheMap.get(key);
    }

    /**
     * @return collection size
     */
    @Override
    public int size() {//O(1)
        return cacheMap.size();
    }
}

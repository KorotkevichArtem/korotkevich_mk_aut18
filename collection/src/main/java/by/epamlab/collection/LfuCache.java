package by.epamlab.collection;

import by.epamlab.collection.interfaces.Cache;

import java.util.*;

/**
 * @author Korotkevich Artem
 */
public class LfuCache<K, V> implements Cache<K, V> {
    // cache will be stored here
    static class Cache<V> {
        private V cacheValues;
        private int frequency;

        public V getCacheValues() {
            return cacheValues;
        }

        public void setCacheValues(V cacheValues) {
            this.cacheValues = cacheValues;
        }

        public int getFrequency() {
            return frequency;
        }

        public void setFrequency(int frequency) {
            this.frequency = frequency;
        }
    }

    private int capacity; //initial capacity
    // a hash map holding <key, Cache> pairs
    private LinkedHashMap<K, Cache> cacheLinkedHashMap = new LinkedHashMap<>();

    /**
     * Create a new LFU cache.
     *
     * @param capacity the size of the cache
     */
    public LfuCache(int capacity) {
        this.capacity = capacity;

    }

    @Override
    public boolean contains(K key) { //O(1)
        return cacheLinkedHashMap.containsKey(key);
    }

    /**
     * Set key to hold the value.
     * If key already holds a value, it is overwritten.
     *
     * @param key   the key of the pair
     * @param value the value of the pair
     */
    @Override
    public V put(K key, V value) {//O(1)
        Cache cache = null;
        if (!cacheLinkedHashMap.containsKey(key)) {
            if (cacheLinkedHashMap.size() == capacity) {
                remove();
            }
            cache = new Cache();
            cache.setCacheValues(value);
            cache.setFrequency(0);
        } else {
            cache = cacheLinkedHashMap.get(key);
            cache.setCacheValues(value);
        }
        return (V) cacheLinkedHashMap.put(key, cache);
    }

    /**
     * searches for the minimum value
     *
     * @return get min Key
     */
    private void remove() {//O(n)
        Map.Entry<K, Cache> min = null;
        for (Map.Entry<K, Cache> entry : cacheLinkedHashMap.entrySet()) {
            if (min == null || min.getValue().getFrequency() > entry.getValue().getFrequency()) {
                min = entry;
            }
        }
        cacheLinkedHashMap.remove(min.getKey());
    }

    /**
     * Get the value of key.
     * If the key does not exist, return -1.
     *
     * @param key the key to query
     * @return the value of the key
     */
    @Override
    public V get(K key) {//O(1)
        if (!cacheLinkedHashMap.containsKey(key)) {
            return null;
        }
        // update frequency
        cacheLinkedHashMap.get(key).frequency++;

        return (V) cacheLinkedHashMap.get(key).getCacheValues();
    }

    /**
     * deletes values ​​by its key
     *
     * @param key key values ​​to be deleted
     * @return boolean flag true or false
     */

    @Override
    public boolean remove(K key) {//O(n)
        if (!cacheLinkedHashMap.containsKey(key)) {
            return false;
        }
        else {
            cacheLinkedHashMap.remove(key);
            return true;
            }
    }

    /**
     * The thread safe method works on the put principle.
     *
     * @param key   object
     * @param value Cache
     * @return value by key
     */
    @Override
    public V putIfAbsent(K key, V value) {//O(n)
        if (!cacheLinkedHashMap.containsKey(key)) {
            return put(key, value);
        }
        return get(key);
    }

    /**
     * @return collection size
     */
    @Override
    public int size() {//O(1)
        return cacheLinkedHashMap.size();
    }
}

package by.epamlab.collection.interfaces;

public interface Cache<K, V> {

    boolean contains(K key);

    V put(K key, V value);

    V get(K key);

    boolean remove(K key);

    V putIfAbsent(K key, V value);

    int size();
}

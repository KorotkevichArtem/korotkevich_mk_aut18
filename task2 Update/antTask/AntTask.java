import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import org.apache.tools.ant.*;

public class AntTask extends Task{
  String fileName;

  public String getFileName() {
      return fileName;
  }

  public void setFileName(String fileName) {
      this.fileName = fileName;
  }

  public class Propriety {
      public Propriety() {
      }

      public String filePropriety;

      public void setFilePropriety(String filePropriety) {
          this.filePropriety = filePropriety;
      }

      public String getFilePropriety() {
          return filePropriety;
      }
  }

  public List<Propriety> proprieties = new ArrayList<>();

  public Propriety createLine() {
      Propriety propriety = new Propriety();
      proprieties.add(propriety);
      return propriety;
  }

  public void execute() {
      try {
          PrintWriter writer = new PrintWriter(fileName, "UTF-8");
          for (Propriety propriety : proprieties) {
              writer.println(propriety.getFilePropriety());
          }
          writer.close();
      } catch (IOException e) {
          throw new BuildException(e);
      }
  }

}  
package by.epamlab.task2.exmpleException;
import java.io.IOException;
import com.budhash.cliche.Command;
import com.budhash.cliche.ShellFactory;

public class Runner {
  @Command
  public void getStackError() {
    try {
     new StackOverflowError();
    }catch (Throwable e) {
      e.printStackTrace();
    }
  }

  @Command
  public void getHeapError() {
    OutOfMemoryErrorHeapSpace space = new OutOfMemoryErrorHeapSpace();
    space.getError();
  }

  @Command
  public void getMetaspaceError() {
    OutOfMemoryErrorMetaspace metaspace = new OutOfMemoryErrorMetaspace();
    metaspace.getError();
  }

  public static void main(String[] args) throws IOException {
    ShellFactory.createConsoleShell("go task", "", new Runner())
    .commandLoop();
  }
}

package by.epamlab.task2.exmpleException;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Map;

public class OutOfMemoryErrorMetaspace {
  private static Map<String, Aint> classLeakingMap = new HashMap<>();
  private static final int DEFAULT_ITERATIONS = 50000;

  public void getError() {
    String[] args = null;
    int nbIterations = (args != null && args.length == 1) ? Integer.parseInt(args[0]) : DEFAULT_ITERATIONS;

    try {

      for (int i = 0; i < nbIterations; i++) {

        String fictiousClassloaderJAR = "file:" + i + ".jar";

        URL[] fictiousClassloaderURL = new URL[] { new URL(fictiousClassloaderJAR) };

        URLClassLoader newClassLoader = new URLClassLoader(fictiousClassloaderURL);

        Aint t = (Aint) Proxy.newProxyInstance(newClassLoader,
            new Class<?>[] { Aint.class },
            (InvocationHandler) new classImplemetsInvocationHandler(new classAImpl()));

        classLeakingMap.put(fictiousClassloaderJAR, t);
      }
    } 
    catch (Throwable any) {
     any.printStackTrace();
    }
  }
}

class classAImpl implements Aint{

  @Override
  public void method(String input) {//    this is a modulation class
  }
}

class classImplemetsInvocationHandler implements InvocationHandler{
  private Object classAImpl;

  public classImplemetsInvocationHandler(Object classAImpl) {
    this.classAImpl = classAImpl;
  }

  @Override
  public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
    if (Object.class == method.getDeclaringClass()) {
      String name = method.getName();
      if ("equals".equals(name)) {
        return proxy == args[0];
      } else if ("hashCode".equals(name)) {
        return System.identityHashCode(proxy);
      } else if ("toString".equals(name)) {
        return proxy.getClass().getName() + "@" +
            Integer.toHexString(System.identityHashCode(proxy)) +
            ", with InvocationHandler " + this;
      } else {
        throw new IllegalStateException(String.valueOf(method));
      }
    }    
    return method.invoke(classAImpl, args);
  }

}

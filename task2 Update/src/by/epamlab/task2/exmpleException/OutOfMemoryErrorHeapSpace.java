package by.epamlab.task2.exmpleException;

import java.util.ArrayList;
import java.util.List;

public class OutOfMemoryErrorHeapSpace {
  public void getError() {
    Integer number = new Integer(1);
    try {
      List<Integer> list = new ArrayList<>();
      while(true) {
        list.add(number + 1);
      }
    }catch (OutOfMemoryError e) {
      throw new OutOfMemoryError();
    }
  }
}



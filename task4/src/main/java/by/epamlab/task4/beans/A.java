package by.epamlab.task4.beans;

import java.util.Objects;

public class A {
    private int anInt = 756245;

    @Override
    public String toString() {
        return "A{" +
                "anInt=" + anInt +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        A a = (A) o;
        return anInt == a.anInt;
    }

    @Override
    public int hashCode() {
        return Objects.hash(anInt);
    }
}

package by.epamlab.task4.beans;

import by.epamlab.task4.annotation.Service;

import java.util.Objects;

@Service
public class Tests {
    private int i = 6;
    private String s = "sss";
    private String getS = "asdasdad";
    @Service
    A a = new A();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tests Tests = (Tests) o;
        return i == Tests.i &&
                Objects.equals(s, Tests.s) &&
                Objects.equals(getS, Tests.getS) &&
                Objects.equals(a, Tests.a);
    }

    @Override
    public int hashCode() {
        return Objects.hash(i, s, getS, a);
    }
}
package by.epamlab.task4;

import by.epamlab.task4.annotation.Service;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class RefTask {

    public static Map<String, Map<String, Object>> toMap(Object object) throws Exception {
        Map<String, Map<String, Object>> classMap = new HashMap<>();
        Map<String, Object> valueMap = new HashMap<>();
        Class<?> clazz = object.getClass();
        Object value = null;
        if (object.getClass().isAnnotationPresent(Service.class)) {
            for (Field field : object.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                if (field.getAnnotation(Service.class) == null) {
                    if (field.getType().equals(String.class)) {
                        valueMap.put(field.getName(), field.get(object));
                    } else if (field.getType().equals(int.class)) {
                        valueMap.put(field.getName(), field.get(object));
                    }
                } else {
                    if (clazz == field.getType()) {
                        throw new StackOverflowError("err");
                    }
                    valueMap.put(field.getName(), toMap(field));
                }
                classMap.put(object.getClass().getName(), valueMap);
            }
        }
        return classMap;
    }

    public static Object fromMap(Map<String, Map<String, Object>> objectMap) throws Exception {
        Object newObject = null;
        for (Map.Entry<String, Map<String, Object>> entry : objectMap.entrySet()) {
            String className = entry.getKey();
            Map<String, Object> value = entry.getValue();
            for (Map.Entry entry1 : value.entrySet()) {
                Object fieldName = entry1.getKey();
                Object fieldValue = entry1.getValue();
                if (fieldValue instanceof Map) {
                    fromMap((Map<String, Map<String, Object>>) fieldValue);
                } else {
                    Class clazz = Class.forName(className);
                    Object o = clazz.newInstance();
                    Field field = clazz.getDeclaredField(fieldName.toString());
                    field.setAccessible(true);
                    field.set(o, fieldValue);
                    newObject = o;
                }
            }
        }
        return newObject;
    }
}

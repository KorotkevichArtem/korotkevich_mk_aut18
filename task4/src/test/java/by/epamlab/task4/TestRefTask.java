package by.epamlab.task4;

import by.epamlab.task4.beans.TestClassRecursion;
import by.epamlab.task4.beans.Tests;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestRefTask {
    RefTask refTask;

    @Test
    public void testEquals() throws Exception {
        Tests tests = new Tests();
        assertEquals(tests, refTask.fromMap(RefTask.toMap(tests)));
    }

    @Test
    public void testInfiniteRecursion() throws Exception {
        TestClassRecursion classRecursion = new TestClassRecursion();
        try {
            refTask.toMap(classRecursion);
        }catch (StackOverflowError error){
            assertTrue(error.getMessage(), true);
        }
    }
}

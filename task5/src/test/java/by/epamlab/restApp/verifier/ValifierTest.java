package by.epamlab.restApp.verifier;

import org.junit.Test;
import static org.junit.Assert.*;

public class ValifierTest {

    private Valifier valifier = new Valifier();

    @Test
    public void testJson(){
        String json1 = "{\"name\":\"a\",\"age\":2}";
        String jsonSchema1 = "{\"definitions\": {},\"$schema\": \"http://json-schema.org/draft-07/schema#\",\"$id\":\"http://example.com/root.json\",\"type\":\"object\",\"title\": \"The Root Schema\",\"required\":[\"name\",\"age\"],\"properties\":{\"name\": {     \"$id\": \"#/properties/name\",\"type\": \"string\",\"title\": \"The Name Schema\",\"default\": \"\",\"examples\": [\"a\"],\"pattern\": \"^(.*)$\"},    \"age\": {\"$id\": \"#/properties/age\",\"type\": \"integer\",\"title\": \"The Age Schema\",\"default\": 0,\"examples\":[2]}}}";
        String json2 = "{\"data\":\"a\",\"number\":2}";
        assertTrue(valifier.validate(jsonSchema1,json1));
        assertFalse(valifier.validate(jsonSchema1,json2));
    }




}

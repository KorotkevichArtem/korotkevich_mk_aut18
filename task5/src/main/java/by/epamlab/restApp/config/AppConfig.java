package by.epamlab.restApp.config;

import by.epamlab.restApp.dao.CollectionDao;
import by.epamlab.restApp.dao.CollectionDaoImpl;
import by.epamlab.restApp.dao.DocumentsDao;
import by.epamlab.restApp.dao.DocumentsDaoImpl;
import by.epamlab.restApp.factory.FactoryForCache;
import by.epamlab.restApp.service.CollectionService;
import by.epamlab.restApp.service.CollectionServiceImpl;
import by.epamlab.restApp.service.DocumentService;
import by.epamlab.restApp.service.DocumentServiceImpl;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;


@Configuration
@PropertySource("${DB_PROPS:classpath:db.properties}")
public class AppConfig {

    @Autowired
    private Environment environment;

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(environment.getRequiredProperty("jdbc.driverClassName"));
        dataSource.setUrl(environment.getRequiredProperty("jdbc.url"));
        dataSource.setUsername(environment.getRequiredProperty("jdbc.username"));
        dataSource.setPassword(environment.getRequiredProperty("jdbc.password"));
        return dataSource;
    }

    @Bean
    @Qualifier("dataSource")
    public JdbcTemplate jdbcTemplate() {
        return new JdbcTemplate(dataSource());

    }

    @Bean
    public CollectionDao collectionDao() {
        return new CollectionDaoImpl(jdbcTemplate());
    }

    @Bean
    public DocumentsDao documentsDao() {
        return new DocumentsDaoImpl(jdbcTemplate());
    }

    @Bean
    public CollectionService collectionService() {
        return new CollectionServiceImpl();
    }

    @Bean
    public DocumentService documentService() {
        return new DocumentServiceImpl();
    }

}

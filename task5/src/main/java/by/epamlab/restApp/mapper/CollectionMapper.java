package by.epamlab.restApp.mapper;

import by.epamlab.restApp.model.Collection;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CollectionMapper implements RowMapper<Collection> {

    public Collection mapRow(ResultSet resultSet, int i) throws SQLException {
        Collection collection = new Collection();
        collection.setName(resultSet.getString("name"));
        collection.setCacheLimit(resultSet.getInt("cachelimit"));
        collection.setAlgorithm(resultSet.getString("algorithm"));
        collection.setJsonSchema(resultSet.getString("jsonschema"));
        return collection;
    }
}

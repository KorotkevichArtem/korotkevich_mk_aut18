package by.epamlab.restApp.mapper;

import by.epamlab.restApp.model.Document;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DocumentMapper implements RowMapper<Document> {

    public Document mapRow(ResultSet resultSet, int i) throws SQLException {
        Document document = new Document();
        document.setKey(resultSet.getString("keys"));
        document.setValue(resultSet.getString("values"));
        document.setName(resultSet.getString("name"));
        return document;
    }
}

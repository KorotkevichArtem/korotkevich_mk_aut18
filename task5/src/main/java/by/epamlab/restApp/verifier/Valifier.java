package by.epamlab.restApp.verifier;


import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

import java.io.IOException;
import java.util.Iterator;

public class Valifier {
    public boolean validate(String jsonSchema, String json) {
        boolean result = false;
        ProcessingReport report = null;
        if (json != null) {
            try {
                JsonNode schemNode = JsonLoader.fromString(jsonSchema);
                JsonNode jsonNode = JsonLoader.fromString(json);
                JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
                JsonSchema schema = factory.getJsonSchema(schemNode);
                report = schema.validate(jsonNode);

            } catch (IOException | ProcessingException e) {
                e.printStackTrace();
            }
            if (report != null) {
                Iterator<ProcessingMessage> iterator = report.iterator();
                while (iterator.hasNext()) {
                    ProcessingMessage message = iterator.next();
                }
                result = report.isSuccess();
            }
        }

        return result;
    }
}

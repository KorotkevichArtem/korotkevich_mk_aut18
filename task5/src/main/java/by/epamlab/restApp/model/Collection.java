package by.epamlab.restApp.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Collection {
    private String name;
    private int cacheLimit;
    private String algorithm;
    private String jsonSchema;

    public Collection() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCacheLimit() {
        return cacheLimit;
    }

    public void setCacheLimit(int cacheLimit) {
        this.cacheLimit = cacheLimit;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public String getJsonSchema() {
        return jsonSchema;
    }

    public void setJsonSchema(String jsonSchema) {
        this.jsonSchema = jsonSchema;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Collection that = (Collection) o;
        return cacheLimit == that.cacheLimit &&
                Objects.equals(name, that.name) &&
                Objects.equals(algorithm, that.algorithm) &&
                Objects.equals(jsonSchema, that.jsonSchema);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, cacheLimit, algorithm, jsonSchema);
    }

    @Override
    public String toString() {
        return "Collection{" +
                "name='" + name + '\'' +
                ", cacheLimit=" + cacheLimit +
                ", algorithm='" + algorithm + '\'' +
                ", jsonSchema='" + jsonSchema + '\'' +
                '}';
    }
}

package by.epamlab.restApp.service;

import by.epamlab.restApp.cache.IСache;
import by.epamlab.restApp.dao.CollectionDao;
import by.epamlab.restApp.factory.FactoryForCache;
import by.epamlab.restApp.model.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;


@Service
public class CollectionServiceImpl implements CollectionService {
    @Autowired
    public CollectionDao collectionDao;
    private LinkedHashMap mapCol = new LinkedHashMap();

    public void create(Collection collection) {
        if (mapCol.containsKey(collection.getName())) {
            mapCol.remove(collection.getName());
            Cache(collection);
        } else {
            Cache(collection);
        }
    }

    public Collection get(String name) {
        if (mapCol.containsKey(name)) {
            IСache IСache = (IСache) mapCol.get(name);
            Collection collection = (Collection) IСache.get(name);
            return collection;
        } else {
            Collection collection = collectionDao.get(name);
            IСache cache = (IСache) FactoryForCache.getCache(collection.getAlgorithm(), collection.getCacheLimit());
            cache.put(collection.getName(), collection);
            mapCol.put(collection.getName(), cache);
            return collection;
        }
    }

    public List getAll(Integer limit, Integer number) {
        return collectionDao.getAll(limit, number);
    }

    public void update(String name, int cacheLimit, String algorithm) {
        Collection collection = collectionDao.get(name);
        collectionDao.update(name, cacheLimit,algorithm);
        IСache IСache = FactoryForCache.getCache(collection.getAlgorithm(), collection.getCacheLimit());
        IСache.put(collection.getName(), collection);
        mapCol.put(collection.getName(), IСache);
    }

    public void delete(String name) {
        if (mapCol.containsKey(name)) {
            mapCol.remove(name);
        }
        collectionDao.delete(name);
    }

    private void Cache(Collection collection) {
        collectionDao.create(collection);
        IСache IСache = FactoryForCache.getCache(collection.getAlgorithm(), collection.getCacheLimit());
        IСache.put(collection.getName(), collection);
        mapCol.put(collection.getName(), IСache);
    }

}

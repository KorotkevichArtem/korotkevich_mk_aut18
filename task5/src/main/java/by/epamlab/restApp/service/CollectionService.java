package by.epamlab.restApp.service;

import by.epamlab.restApp.model.Collection;

import java.util.List;

public interface CollectionService<T> {
    void create(Collection collection);

    T get(String name);

    List<T> getAll(Integer limit, Integer number);

    void update(String name, int cacheLimit, String algorithm);

    void delete(String name);

}

package by.epamlab.restApp.service;

import by.epamlab.restApp.cache.IСache;
import by.epamlab.restApp.dao.CollectionDao;
import by.epamlab.restApp.dao.DocumentsDao;
import by.epamlab.restApp.factory.FactoryForCache;
import by.epamlab.restApp.model.Collection;
import by.epamlab.restApp.model.Document;
import by.epamlab.restApp.verifier.Valifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;

@Service
public class DocumentServiceImpl implements DocumentService {

    @Autowired
    public CollectionDao collectionDao;
    @Autowired
    public DocumentsDao documentsDao;

    private Valifier valifier = new Valifier();
    private LinkedHashMap map = new LinkedHashMap();

    public void create(Document document) {
        Collection collection = collectionDao.get(document.getName());
        if (valifier.validate(collection.getJsonSchema(), document.getValue())) {
            if (map.containsKey(document.getKey())) {
                map.remove(document.getKey());
                Cache(document, collection);
            } else {
                Cache(document, collection);
            }
        } else {
            throw new IllegalArgumentException("json invalid");
        }
    }

    public Document get(String name) {
        if (map.containsKey(name)) {
            IСache IСache = (IСache) map.get(name);
            Document document = (Document) IСache.get(name);
            return document;
        } else {
            Document document = (Document) documentsDao.get(name);
            Collection collection = collectionDao.get(document.getName());
            IСache cache = FactoryForCache.getCache(collection.getAlgorithm(), collection.getCacheLimit());
            cache.put(document.getKey(), document);
            map.put(document.getKey(), cache);
            System.out.println(map);
            return document;
        }
    }

    public List getAll(Integer limit, Integer number) {
        return documentsDao.getAll(limit, number);
    }


    public void update(String name, String value) {
            documentsDao.update(name, value);
            Document doc = (Document) documentsDao.get(name);
            Collection collection = collectionDao.get(doc.getName());
            IСache cache = FactoryForCache.getCache(collection.getAlgorithm(), collection.getCacheLimit());
            cache.put(doc.getKey(), doc.getValue());
            map.put(doc.getKey(), cache);

    }

    public void delete(String name) {
        documentsDao.delete(name);
        if (map.containsKey(name)) {
            map.remove(name);
        }

    }
    private void Cache(Document document, Collection collection) {
        documentsDao.create(document);
        IСache cache = FactoryForCache.getCache(collection.getAlgorithm(), collection.getCacheLimit());
        cache.put(document.getKey(), document);
        map.put(document.getKey(), cache);
    }
}

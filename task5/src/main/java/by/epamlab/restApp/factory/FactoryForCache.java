package by.epamlab.restApp.factory;

import by.epamlab.restApp.cache.CacheLfuImpl;
import by.epamlab.restApp.cache.CacheLruImp;
import by.epamlab.restApp.cache.IСache;

public class FactoryForCache {
    public static IСache<String, Object> getCache(String algorithm, int cacheLimit) {
        if (algorithm.equalsIgnoreCase("LRU") ) {
            return new CacheLruImp<>(cacheLimit);
        }else if (algorithm.equalsIgnoreCase("LFU")){
            return new CacheLfuImpl<>(cacheLimit);
        }
        else {
            throw new IllegalArgumentException("don't found algorithm cache");
        }
    }
}



package by.epamlab.restApp.loadBalancer;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.simple.parser.JSONParser;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JsonPars {

    public Map<?, List<?>> getJsonList() throws IOException {
        HashMap nodeList = new HashMap<>();
        JSONParser parser = new JSONParser();

        ObjectMapper objectMapper = new ObjectMapper();
        nodeList = objectMapper.readValue(Files.readAllBytes(Paths.get("src/main/resources/cloud.json")), HashMap.class);

        return nodeList;
    }
}

package by.epamlab.restApp.dao;

import by.epamlab.restApp.mapper.CollectionMapper;
import by.epamlab.restApp.model.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CollectionDaoImpl implements CollectionDao {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public CollectionDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void create(Collection collection){
        String sql = "INSERT INTO collections(name,cachelimit,algorithm,jsonschema) VALUES (?,?,?,?);";
        jdbcTemplate.update(sql,collection.getName(),collection.getCacheLimit(),collection.getAlgorithm(),collection.getJsonSchema());
    }

    @Override
    public Collection get(String name) {
        String sql = "select * from  finalproject.collections where finalproject.collections.name = ?;";
        Collection collection = jdbcTemplate.queryForObject(sql, new CollectionMapper(), name);
        return collection;
    }

    public List getAll(Integer limit, Integer number) {
        String sql = "SELECT * FROM finalproject.collections limit ? offset ?;";
        return jdbcTemplate.query(sql, new CollectionMapper(),limit,number);
    }

    public void update(String name, int cacheLimit, String algorithm) {
        String sql = "UPDATE `finalproject`.`collections` SET `cachelimit`=?, `algorithm`=? WHERE `name`= ?;";
        jdbcTemplate.update(sql, cacheLimit, algorithm, name);
    }

    public void delete(String name) {
        String sql = " DELETE FROM collections WHERE name = ?;";
        jdbcTemplate.update(sql, name);
    }
}

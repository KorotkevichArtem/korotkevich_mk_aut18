package by.epamlab.restApp.dao;

import by.epamlab.restApp.model.Collection;

import java.util.List;

public interface CollectionDao<T> {//убрать <>
    void create(Collection collection);

    Collection get(String name);

    List<T> getAll(Integer limit, Integer number);

    void update(String name, int cacheLimit, String algorithm);

    void delete(String name);

}

package by.epamlab.restApp.dao;

import by.epamlab.restApp.mapper.DocumentMapper;
import by.epamlab.restApp.model.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DocumentsDaoImpl implements DocumentsDao {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public DocumentsDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void create(Document document) {
        String sql = "INSERT INTO `finalproject`.`documents` (`keys`, `values`, `name`) VALUES (?,?,?);";
        jdbcTemplate.update(sql, document.getKey(), document.getValue(),document.getName());
    }

    public Document get(String name) {
        String sql = "select * from  finalproject.documents where finalproject.documents.keys = ?;";
        Document document = jdbcTemplate.queryForObject(sql, new DocumentMapper(), name);
        return document;
    }

    public List getAll(Integer limit, Integer number) {
        String sql = "SELECT * FROM finalproject.documents limit ? offset ?;";
        return jdbcTemplate.query(sql, new DocumentMapper(), limit, number);
    }

    public void update(String name, String value) {
        String sql = "UPDATE `finalproject`.`documents` SET `values`= ? WHERE `keys`=?;";
        jdbcTemplate.update(sql, value, name);

    }

    public void delete(String name) {
        String sql = "DELETE FROM finalproject.documents WHERE documents.keys = ?;";
        jdbcTemplate.update(sql, name);
    }
}

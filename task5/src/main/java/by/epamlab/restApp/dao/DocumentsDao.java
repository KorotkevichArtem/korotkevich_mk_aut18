package by.epamlab.restApp.dao;

import by.epamlab.restApp.model.Document;

import java.util.List;

public interface DocumentsDao<T> {//warning <?>
    void create(Document document);

    T get(String name);

    List<T> getAll(Integer limit, Integer number);

    void update(String name, String value);

    void delete(String name);
}

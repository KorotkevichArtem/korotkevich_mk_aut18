package by.epamlab.restApp.controller;

import by.epamlab.restApp.model.Collection;
import by.epamlab.restApp.model.Document;
import by.epamlab.restApp.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/")
public class DocumentsController {

    @Autowired
    public DocumentService documentService;

    /*---Add new document---*/
    @PostMapping("/document")
    public ResponseEntity<?> saveDocument(@RequestBody Document document) {
        System.out.println(document.getKey() + document.getValue() + document.getKey());
        documentService.create(document);
        return ResponseEntity.ok().body("New Collection has been saved with ID:" + document.getKey());
    }

    /*---Get a document by id---*/
    @GetMapping("/document/{name}")
    public ResponseEntity<?> getDocument(@PathVariable("name") String name) {
       Document document = (Document) documentService.get(name);
        return ResponseEntity.ok(document);
    }

    /*---get all document---*/
    @GetMapping("/document/{limit}/{number}")
    @ResponseBody
    public ResponseEntity<List<Document>> list(@PathVariable("limit") Integer limit, @PathVariable( "number") Integer number) {
        List<Document> documentList = documentService.getAll( limit, number);
        return ResponseEntity.ok().body(documentList);
    }

    /*---Update a document by id---*/
    @PutMapping("/document/{key}")
    @ResponseBody
    public ResponseEntity<?> update(@PathVariable("key") String key, @RequestBody  String value) {
        documentService.update(key,value);
        return ResponseEntity.ok().body("Collection has been updated successfully.");
    }

    /*---Delete a collection by id---*/
    @DeleteMapping("/document/{key}")
    @ResponseBody
    public ResponseEntity<?> delete(@PathVariable("key") String key) {
        documentService.delete(key);
        return ResponseEntity.ok().body("Collection has been deleted successfully.");
    }
}

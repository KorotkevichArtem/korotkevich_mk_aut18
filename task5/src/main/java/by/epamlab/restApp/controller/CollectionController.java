package by.epamlab.restApp.controller;

import by.epamlab.restApp.model.Collection;
import by.epamlab.restApp.service.CollectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

@Controller
@RequestMapping("/")

public class CollectionController {

    @Autowired
    public CollectionService collectionService;

    /*---Add new collection---*/
    @PostMapping("/collection")
    public ResponseEntity<?> saveCollection(@RequestBody Collection collection) {
        collectionService.create(collection);
        return ResponseEntity.ok().body("New Collection has been saved with ID:" + collection.getName());
    }

    /*---Get a collection by id---*/
    @GetMapping("/collection/{name}")
    public ResponseEntity<?> getCollection(@PathVariable("name") String name) {
        Collection collection = (Collection) collectionService.get(name);
        return ResponseEntity.ok(collection);
    }

    /*---get all collection---*/
    @GetMapping("/collection/{limit}/{number}")
    @ResponseBody
    public ResponseEntity<List<Collection>> list(@PathVariable("limit") Integer limit, @PathVariable( "number") Integer number) throws UnknownHostException {
        List<Collection> collectionList = collectionService.getAll(limit,number);
        System.out.println(InetAddress.getLocalHost().getHostName());
        System.out.println(InetAddress.getLocalHost().getHostAddress());
        return ResponseEntity.ok().body(collectionList);
    }

    /*---Update a collection by id---*/
    @PutMapping("/collection/{name}")
    @ResponseBody
    public ResponseEntity<?> update(@PathVariable("name") String name, @RequestBody Collection collection) {
        collectionService.update(name, collection.getCacheLimit(), collection.getAlgorithm());
        return ResponseEntity.ok().body("Collection has been updated successfully.");
    }

    /*---Delete a collection by id---*/
    @DeleteMapping("/collection/{name}")
    @ResponseBody
    public ResponseEntity<?> delete(@PathVariable("name") String name) {
        collectionService.delete(name);
        return ResponseEntity.ok().body("Collection has been deleted successfully.");
    }
}

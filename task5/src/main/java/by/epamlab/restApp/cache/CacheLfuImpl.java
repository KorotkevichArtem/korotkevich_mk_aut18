package by.epamlab.restApp.cache;

import java.util.LinkedHashMap;
import java.util.Map;

public class CacheLfuImpl<K, V> implements IСache<K, V> {

    static class Cache<V> {
        private V cacheValues;
        private int frequency;

        public V getCacheValues() {
            return cacheValues;
        }

        public void setCacheValues(V cacheValues) {
            this.cacheValues = cacheValues;
        }

        public int getFrequency() {
            return frequency;
        }

        public void setFrequency(int frequency) {
            this.frequency = frequency;
        }
    }

    private int capacity;
    private LinkedHashMap<K, Cache> cacheLinkedHashMap = new LinkedHashMap<>();

    public CacheLfuImpl(int capacity) {
        this.capacity = capacity;

    }

    public synchronized boolean contains(K key) {
        Cache cache = this.cacheLinkedHashMap.get(key);
        return cache != null;
    }

    public synchronized V put(K key, V value) {
        Cache cache = null;
        if (!cacheLinkedHashMap.containsKey(key)) {
            if (cacheLinkedHashMap.size() == capacity) {
                remove();
            }
            cache = new Cache();
            cache.setCacheValues(value);
            cache.setFrequency(0);
        } else {
            cache = cacheLinkedHashMap.get(key);
            cache.setCacheValues(value);
        }
        return (V) cacheLinkedHashMap.put(key, cache);
    }

    private synchronized void remove() {
        Map.Entry<K, Cache> min = null;
        for (Map.Entry<K, Cache> entry : cacheLinkedHashMap.entrySet()) {
            if (min == null || min.getValue().getFrequency() > entry.getValue().getFrequency()) {
                min = entry;
            }
        }
        cacheLinkedHashMap.remove(min.getKey());
    }

    public synchronized V get(K key) {
        if (!cacheLinkedHashMap.containsKey(key)) {
            return null;
        }
        cacheLinkedHashMap.get(key).frequency++;

        return (V) cacheLinkedHashMap.get(key).getCacheValues();
    }

    public synchronized boolean remove(K key) {
        boolean res;
        if (!cacheLinkedHashMap.containsKey(key)) {
            res = false;
        } else {
            cacheLinkedHashMap.remove(key);
            res = true;
        }
        return res;
    }


    public synchronized V putIfAbsent(K key, V value) {
        if (!cacheLinkedHashMap.containsKey(key)) {
            return put(key, value);
        }
        return get(key);
    }


    public synchronized int size() {
        return cacheLinkedHashMap.size();
    }
}


package by.epamlab.restApp.cache;

import java.util.Iterator;
import java.util.LinkedHashMap;

public class CacheLruImp<K, V> implements IСache<K, V> {
    private int capacti;
    private LinkedHashMap<K, V> cacheMap;

    public CacheLruImp(int capacti) {
        this.capacti = capacti;
        this.cacheMap = new LinkedHashMap<>();
    }


    public boolean contains(K key) {
        return this.cacheMap.containsKey(key);
    }

    public V put(K key, V value) {
        if (!contains(key)) {
            if (this.cacheMap.size() == capacti) {
                remove();
            }
        }
        synchronized (cacheMap){
            return cacheMap.put(key, value);
        }
    }

    public synchronized void remove() {
        Iterator<?> iterator = this.cacheMap.keySet().iterator();
        iterator.next();
        iterator.remove();
    }

    public V get(K key) {
        if (contains(key)) {
            V value = cacheMap.remove(key);
            cacheMap.put(key, value);
            synchronized (cacheMap) {
                return cacheMap.get(key);
            }
        } else {
            return null;
        }
    }

    public synchronized boolean remove(K key) {
        boolean result;
        Object value = this.cacheMap.get(key);
        if (value == null) result = false;
        else {
            cacheMap.remove(key);
            result = true;
        }
        return result;
    }

    public synchronized V putIfAbsent(K key, V value) {
        if (!cacheMap.containsKey(key)) {
            return cacheMap.put(key, value);
        }
        return  cacheMap.get(key);
    }

    public synchronized int size() {//O(1)
        synchronized (cacheMap) {
            return cacheMap.size();
        }
    }
}